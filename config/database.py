import os
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.declarative import declarative_base #Para manipular las tablas de la base de datos

#Nombre de la base de datos
sqlite_file_name = "../database.sqlite"

#Se lee el directorio del archivo actual (database.py)
base_dir = os.path.dirname(os.path.realpath(__file__))

#URL base de datos
data_base_url = f"sqlite:///{os.path.join(base_dir, sqlite_file_name)}"

#Motor de la base de datos
engine = create_engine(data_base_url, echo = True)

#Crear una sesión para conectarse a la base de datos
Session = sessionmaker(bind = engine)

Base = declarative_base()

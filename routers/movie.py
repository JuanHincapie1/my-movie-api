from fastapi import APIRouter
from fastapi import Path, Query, Depends
from fastapi.responses import JSONResponse
from fastapi.security.http import HTTPAuthorizationCredentials #PARA PORDER RETORNAR HTMLS
from pydantic import BaseModel, Field
from typing import Optional, List
from starlette.requests import Request
from config.database import Session
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from middlewords.jwt_bearer import JWTBearer
from services.movie import MovieService
from schemas.movie import Movie

movie_router = APIRouter()


@movie_router.get('/movies', tags =['movies'], response_model=List[Movie], status_code=200, dependencies=[Depends(JWTBearer())])
#Se crea la función que tiene el listado de peliculas
def get_movies() -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(status_code=200,content=jsonable_encoder(result))




#Para recibir parámetros, se hace mediante el siguiente código, en este caso, se 
##desea el parámetro id
@movie_router.get('/movies/{id}', tags =['movies'], response_model=Movie)
#Creamos nuestra función indicando que párametro va a recibir (en este caso id) y
#de que tipo es (en este caso int)
def get_movie(id: int = Path(ge = 1, le = 2000)) -> Movie:
    #Se realiza un for que recorra el items
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={'message:': "No encontrado"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

#Cuando se obtiene un parametro query se debe añadir una barra al final después del
#get, por otra parte, se sabe que es un query, ya que en la función, cuando enviamos un
#parámetro que no esta definido en el app.get, se entiende que es un query
@movie_router.get('/movies/', tags =['movies'], response_model=Movie)

def get_movies_by_category(category: str = Query(min_length=5, max_length=15)) -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movie_by_category(category)
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

#----------------------METODO_POST-----------------------

#Para que no detecte como un query, sino como un body, debemos importar Body (AL INICIO DEL
#CÓDIGO) y en la función, cuando se ingresan las variables, se le agrega el siguiente 
#contenido (= Body())
@movie_router.post('/movies', tags = ['movies'], response_model=dict,status_code=201)

#Todos los parámetros que hemos definido previamente, se deben añadir en la función
def create_movie(movie: Movie) -> dict:
    db = Session()
    MovieService(db).create_movie(movie)  
    return JSONResponse(status_code=201, content={"message": "Se ha registrado la pelicula"})
    
    
#-----------------------METODO PUT Y DELETE --------------------------------

@movie_router.put('/movies/{id}', tags=['movies'], response_model=dict, status_code=200)
def update_movie(id: int, movie: Movie) -> dict:
    
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={'message:': "No encontrado"})
    MovieService(db).update_movie(id, movie)
    return JSONResponse(status_code=200, content={"message": "Se ha modificado la pelicula"})
       
@movie_router.delete('/movies/{id}', tags=['movies'], response_model=dict, status_code=200)
def delete_movie(id: int) -> dict:
    
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={'message:': "No encontrado"})
    return JSONResponse(status_code=200, content={"message": "Se ha eliminado la pelicula"})
